\import Algebra.Meta
\import Algebra.Monoid
\import Arith.Nat
\import Data.List
\import Data.Or
\import Function.Meta
\import Logic.Meta
\import Meta
\import Order.LinearOrder
\import Order.StrictOrder
\import Paths (pmap2)
\import Paths.Meta
\import Utils (*>, <->, Maybe, just, length, maybe, nothing, pmap, sym)
\import hw05
\import lect05 (funExt)
\import lect07(Decide, no, yes)
\import lect08
\open LinearOrder

-- 1. Докажите, что Perm l1 l2 тогда и только тогда, когда Insertion.sort l1 = Insertion.sort l2.
\func perm-sort-iff {A : Dec} (l1 l2 : List A) : Perm l1 l2 <-> (Insertion.sort l1 = Insertion.sort l2) => (to, from)
  \where {
    \func to {A : Dec} {l1 l2 : List A} (p : Perm l1 l2) : Insertion.sort l1 = Insertion.sort l2 \elim l1, l2, p
      | nil, nil, perm-nil => idp
      | :: x xs, :: y ys, perm-:: p p1 => run {
        rewrite p,
        pmap (Insertion.sort.insert y) (to p1)
      }
      | :: x (:: x' xs), :: y (:: y' ys), perm-swap p p1 p2 => run {
        rewrite p,
        rewrite p1,
        rewrite p2,
        Insertion.sort-perm.insert-comm y' y (Insertion.sort ys)
      }
      | xs, ys, perm-trans p1 p2 => to p1 *> to p2
    \func from {A : Dec} {l1 l2 : List A} (p : Insertion.sort l1 = Insertion.sort l2) : Perm l1 l2 \elim l1, l2, p
      | nil, nil, p => perm-nil
      | nil, :: a l2, p => run {
        \let p1 => Insertion.sort-perm.insert-perm a (Insertion.sort l2),
        rewrite p,
        perm-sym,
        \let p2 => perm-sortTail a l2,
        perm-trans p2 p1
      }
      | :: a l1, nil, p => run {
        \let p1 => Insertion.sort-perm.insert-perm a (Insertion.sort l1),
        rewrite (sym p),
        \let p2 => perm-sortTail a l1,
        perm-trans p2 p1
      }
      | :: a l1, :: a1 l2, p => run {
        \let p1 => Insertion.sort-perm.insert-perm a (Insertion.sort l1),
        \let p2 => Insertion.sort-perm.insert-perm a1 (Insertion.sort l2),
        rewrite p at p1,
        \let p3 => perm-trans p1 (perm-sym p2),
        \let p4 => perm-sortTail a l1,
        \let p5 => perm-sym (perm-sortTail a1 l2),
        perm-trans p4 (perm-trans p3 p5)
      }
    \func perm-sortTail {A : Dec} (a : A) (l : List A) : Perm (a :: l) (a :: Insertion.sort l) => perm-:: idp (Insertion.sort-perm l)
  }

\func perm-sym {A : \Type} {xs ys : List A} (p : Perm xs ys) : Perm ys xs
  | {A}, {nil}, {nil}, perm-nil => perm-nil
  | {A}, {:: x xs}, {:: y ys}, perm-:: p p1 => perm-:: (sym p) (perm-sym p1)
  | {A}, {:: x (:: x' xs)}, {:: y (:: y' ys)}, perm-swap p p1 p2 => perm-swap (sym p1) (sym p) (sym p2)
  | perm-trans p1 p2 => perm-trans (perm-sym p2) (perm-sym p1)

-- 2. Докажите, что спецификация "результат является перестановкой входа и отсортирован" является полной для функции sort.

\func completeness {A : Dec} (sort : List A -> List A) (l : List A) (p : Perm l (sort l)) (s : Sorted (sort l)) : sort l = Insertion.sort l =>
  \let
    | p1 : Perm (Insertion.sort l) (sort l) => perm-trans (perm-sym (Insertion.sort-perm l)) p
    | s2 : Sorted (Insertion.sort l) => Insertion.sort-sorted l
  \in sym (eq-sorted-perm p1 s2 s)
  \where {
    \func eq-sorted-perm {A : Dec} {l1 l2 : List A} (p : Perm l1 l2) (s1 : Sorted l1) (s2 : Sorted l2) : l1 = l2 =>
      \let
        | f => (perm-sort-iff l1 l2).1
        | eq => f p
      \in run {
        rewrite (eq-sort-sorted s1),
        rewrite (eq-sort-sorted s2),
        eq
      }
    \func eq-sort-sorted {A : Dec} {l : List A} (s : Sorted l) : l = Insertion.sort l
      | {A}, {nil}, sorted-nil => idp
      | {A}, {:: x xs}, sorted-cons _x s =>
        \let
          | eq => eq-sort-sorted s
        \in run {
          rewriteI eq,
          insert-min-to-sorted _x s
        }
    \func insert-min-to-sorted {A : Dec} {x : A} {xs : List A} (x<= : x <= headDef x xs) (s : Sorted xs) : x :: xs = Insertion.sort.insert x xs
      | {A}, {x}, {nil}, <=-equals p, sorted-nil => idp
      | {A}, {x}, {nil}, <=-less _x, sorted-nil => idp
      | {A}, {x}, {:: x1 xs}, <=-equals p, sorted-cons _x s => mcases \with {
        | inl x<a => run {
          rewrite p,
          \let kek => insert-min-to-sorted _x s,
          rewriteI kek,
          idp
        }
        | inr a<=x => idp
      }
      | {A}, {x}, {:: x1 xs}, <=-less x<x1, sorted-cons _x1 s => mcases \with {
        | inl x1<x => contradiction (<-irreflexive (<-transitive x<x1 x1<x))
        | inr a<=x => idp
      }
  }


-- 3. Определите факториал через хвостовую рекурсию.
--    Докажите, что он равен обычному определению факториала.

\func fac'-a (n acc : Nat) : Nat \elim n
  | 0 => acc
  | suc n => fac'-a n (acc * suc n)

\func fac' (n : Nat) => fac'-a n 1

\func fac (x : Nat) : Nat \elim x
  | 0 => 1
  | suc x => (suc x) * (fac x)

\func fac=fac'-a (n a : Nat) : a * fac n = fac'-a n a \elim n
  | 0 => idp
  | suc n => run {
    rewrite (fac=fac'-a n (suc n)),
    sym (remove n a)
  }
  \where {
    \func remove (n a : Nat) : fac'-a n (a * suc n) = a * fac'-a n (suc n) \elim n
      | 0 => idp
      | suc n => run {
        rewrite (remove n (suc (suc n))),
        \let x : a * (suc (suc n) * fac'-a n (suc n)) = a * suc (suc n) * fac'-a n (suc n) => sym *-assoc,
        rewrite x,
        remove n (a * suc (suc n))
      }
  }

\func fac=fac' (n : Nat) : fac n = fac' n => run {
  unfold fac',
  rewrite (sym (fac=fac'-a n 1)),
  \let p : 1 * fac n = fac n => *-comm,
  rewrite p,
  idp
}

-- 4. Закончите определение \instance Monad для Term.

\class Functor (F : \Type -> \Type)
  | fmap {A B : \Type} (f : A -> B) : F A -> F B
  | fmap-id {A : \Type} (y : F A) : fmap (\lam (x : A) => x) y = y
  | fmap-comp {A B C : \Type} (f : A -> B) (g : B -> C) (y : F A)
  : fmap (\lam x => g (f x)) y = fmap g (fmap f y)

\class Monad \extends Functor
  | return {A : \Type} : A -> F A
  | \infixl 1 >>= {A B : \Type} : F A -> (A -> F B) -> F B
  | >>=-left {A B : \Type} (a : A) (k : A -> F B) : (return a >>= k) = k a
  | >>=-right {A : \Type} (m : F A) : (m >>= return) = m
  | >>=-assoc {A B C : \Type} (m : F A) (k : A -> F B) (l : B -> F C) : ((m >>= k) >>= l) = (m >>= (\lam a => k a >>= l))
  | fmap => \lam f => (\lam ma => ma >>= (\lam x => return (f x)))
  | fmap-id => \lam {A} y => >>=-right y
  | fmap-comp {A B C : \Type} f g y =>
    sym (>>=-assoc y (\lam x => return (f x)) (\lam z => return (g z))
      *> path (\lam i => y >>= (\lam x => >>=-left (f x) (\lam z => return (g z)) @ i)))

\instance MaybeMonad : Monad Maybe
  | return => just
  | >>= m k => maybe nothing k m
  | >>=-left => \lam {A} a f => idp
  | >>=-right {A : \Type} (m : Maybe A) => cases m idp
  | >>=-assoc {A B C : \Type} (m : Maybe A) (k : A -> Maybe B) (l : B -> Maybe C) => cases m idp

\func termMap {A B : \Type} (f : A -> B) (t : Term A) : Term B \elim t
  | var v => var (f v)
  | unit => unit
  | app t1 t2 => app (termMap f t1) (termMap f t2)
  | lam t => lam (termMap (maybe nothing (\lam x => just (f x))) t)

{-
-- это некорректное определение termMap just
\func lift {A : \Type} (t : Term A) : Term (Maybe A) \elim t
  | var v => var (just v)
  | unit => unit
  | app t1 t2 => app (termMap just t1) (termMap just t2)
  | lam t => lam (termMap just t)
-}

\func liftM {A B : \Type} (k : A -> Term B) (m : Maybe A) : Term (Maybe B)
  => maybe (var nothing) (\lam a => termMap just (k a)) m


\instance TermMonad : Monad Term
  | return => var
  | >>= {A B : \Type} (t : Term A) (k : A -> Term B) : Term B \elim t {
    | var v => k v
    | unit => unit
    | app t1 t2 => app (t1 >>= k) (t2 >>= k)
    | lam t => lam (t >>= liftM k)
  }
  | >>=-left {A B : \Type} (a : A) (k : A -> Term B) => idp
  | >>=-right => >>=-right-impl
  | >>=-assoc => >>=-assoc-impl
  \where {
    \func >>=-right-impl {A : \Type} (t : Term A) : (t >>= var) = t
      | var v => idp
      | unit => idp
      | app t1 t2 => pmap2 app (>>=-right-impl t1) (>>=-right-impl t2)
      | lam t => pmap lam (>>-right-liftM t)
    \func >>=-assoc-impl {A B C : \Type} (t : Term A) (k : A -> Term B) (l : B -> Term C) : ((t >>= k) >>= l) = (t >>= (\lam a => k a >>= l)) \elim t
      | var v => idp
      | unit => idp
      | app t1 t2 => pmap2 app (>>=-assoc-impl t1 k l) (>>=-assoc-impl t2 k l)
      | lam t => run {
        rewrite (aux3 k l),
        rewrite (>>=-assoc-impl t (liftM k) (liftM l)),
        idp
      }

    \func >>-right-liftM {A : \Type} (t : Term (Maybe A)) : (t >>= liftM var) = t
      | var v => cases v (unfold liftM idp)
      | unit => idp
      | app t1 t2 => pmap2 app (>>-right-liftM t1) (>>-right-liftM t2)
      | lam t => run {
        rewrite aux2,
        rewrite aux2,
        pmap lam (>>=-right-impl t)
      }
    \func aux2 {A : \Type} : liftM var = var => funExt (\lam _ => Term (Maybe A)) (\lam x => cases x idp)
    \func aux3 {A B C : \Type} (k : A -> Term B) (l : B -> Term C) : liftM (\lam a => k a >>= l) = (\lam a => (liftM k) a >>= liftM l) => funExt (\lam _ => Term (Maybe C))  (\lam x => \case \elim x \with {
      | nothing => idp
      | just a => run {
        rewrite (ff a (\lam a => k a >>= l)),
        rewrite (ff a k),
        gg (k a) l
      }
    } )
    \func gg {A B : \Type} (t : Term A) (l : A -> Term B) : (termMap just (t >>= l)) = (termMap just t >>= liftM l) \elim t
      | var v => idp
      | unit => idp
      | app t1 t2 => {?} -- pmap2 app (gg t1 l) (gg t2 l)
      | lam t => pmap lam {?}

    \func ff {A B : \Type} (a : A) (f : A -> Term B) : liftM f (just a) = termMap just (f a) => idp
    \func kk {A : \Type} : termMap (maybe {A} nothing (\lam x => just (just x))) = termMap just => funExt (\lam _ => Term (Maybe (Maybe A))) (\lam x => \case \elim x \with {
      | var v => \case \elim v \with {
        | nothing => {?}
        | just a => {?}
      }
      | unit => {?}
      | app x1 x2 => {?}
      | lam x => {?}
    })

    --\func kk2 {A B : \Type} (t : Term (Maybe A)) (l : A -> Term B) : termMap (maybe nothing (\lam x => just (just x))) (t >>= liftM l) = termMap just (t >>= l) => {?}

    --\func kk3 {A B : \Type} (t : Term A) (l : A -> Term (B)) : (termMap (maybe nothing (\lam x => just (just x))) t >>= liftM (liftM l)) = (termMap just t >>= liftM l) => {?}
  }


-- 5. Определите отношение многошаговой редукции на Term.


\func Red {V : \Type} (t s : Term V) : \Type => ReflTransClosure Red1 t s

-- 6. Стратегия редукции -- это функция, которая каждому терму сопоставляет либо некоторый терм, к которому он редуцируется за 1 шаг, либо доказательство, что таких термов нет.
--    Определите любую стратегию редукции.

\func strategy {V : \Type} (t : Term V) : Decide (\Sigma (s : Term V) (Red1 t s)) \elim t
  | var v => no (\case __.2)
  | unit => no (\case __.2)
  | app (lam t1) t2 => yes (t1 >>= maybe t2 var, beta t1 idp idp)
  | app (var v) t => \case strategy t \with {
    | yes (s, t=>s) => yes (app (var v) s, appRight idp t=>s)
    | no _x => no (\lam _x1 => \case _x1 \with {
      | (app (var v) s, appRight p r) => _x (s, r)
      | (var v1,r) => cases r contradiction
      | (unit,r) => cases r contradiction
      | (lam t1,r) => cases r contradiction
      | (app unit t1,r) => cases r contradiction
      | (app (app t1 t2) t3,r) => cases r contradiction
      | (app (lam t1) t2,r) => cases r contradiction
      | (app (var v1) s,appLeft r p) => contradiction
      | (app (var v1) s,beta f p p1) => contradiction
    })
  }
  | app unit t => \case strategy t \with {
    | yes (s, t=>s) => yes (app unit s, appRight idp t=>s)
    | no _x => no (\lam _x1 => \case _x1 \with {
      | (app unit s, appRight p r) => _x (s, r)
      | (var v,r) => cases r contradiction
      | (unit,r) => cases r contradiction
      | (lam t1,r) => cases r contradiction
      | (app (var v) t1,r) => cases r contradiction
      | (app (app t1 t2) t3,r) => cases r contradiction
      | (app (lam t1) t2,r) => cases r contradiction
      | (app unit s,appLeft r p) => contradiction
      | (app unit s,beta f p p1) => contradiction
    })
  }
  | app ((app t11 t12) \as t1) t2 => \case strategy t1 \with {
    | yes (s, t1=>s) => yes (app s t2, appLeft t1=>s idp)
    | no leftFailed => \case strategy t2 \with {
      | yes (s, t2=>s) => yes (app t1 s, appRight idp t2=>s)
      | no rightFailed => no (\lam _x => \case _x \with {
        | (app s t2, appLeft r p) => leftFailed (s, r)
        | (var v,r) => cases r contradiction
        | (unit,r) => cases r contradiction
        | (lam t,r) => cases r contradiction
        | (app s t3,appRight p r) => rightFailed (t3, r)
        | (app s t3,beta f p p1) => contradiction
      })
    }
  }
  | lam t => \case strategy t \with {
    | yes (s, t=>s) => yes (lam s, lamRed t=>s)
    | no t=/>s => no (\lam _x1 => \case _x1 \with {
      | (lam s, lamRed r) => t=/>s (s, r)
      | (var v,r) => contradiction
      | (unit,r) => contradiction
      | (app t1 t2,r) => contradiction
    })
  }

-- 7. Докажите, что подтермы достижимого терма достижимы.

\func appAccLeft {V : \Type} {t s : Term V} (a : Acc Red1 (app t s)) : Acc Red1 t \elim a
  | acc f => acc (\lam t' t=>t' => \let ts=>t's : Red1 (app t s) (app t' s) => appLeft t=>t' idp \in
    appAccLeft (f (app t' s) ts=>t's))

\func appAccRight {V : \Type} {t s : Term V} (a : Acc Red1 (app t s)) : Acc Red1 s \elim a
  | acc f => acc (\lam s' s=>s' => \let ts=>ts' : Red1 (app t s) (app t s') => appRight idp s=>s' \in
    appAccRight (f (app t s') ts=>ts'))

\func subLamAcc {V : \Type} {t : Term (Maybe V)} (a : Acc Red1 (lam t)) : Acc Red1 t \elim a
  | acc f => acc (\lam t' t=>t' => \let lamt=>lamt' : Red1 (lam t) (lam t') => lamRed t=>t' \in
    subLamAcc (f (lam t') lamt=>lamt'))

-- 8. Докажите следующую обобщенную лемму о подстановке и выведите из нее обычную версию: если Г, x : A |- b : B и Г |- a : A, то Г |- b[a/x] : B.

\func substLem {U V : \Type} (ctx : U -> Type) (b : Term U) (B : Type) (h : hasType ctx b B)
               (ctx' : V -> Type) (a : U -> Term V) (h' : \Pi (u : U) -> hasType ctx' (a u) (ctx u))
  : hasType ctx' (b >>= a) B
  => {?}

\func substLem1 {V : \Type} {ctx : V -> Type} (a : Term V) {A : Type} (ha : hasType ctx a A)
                (b : Term (Maybe V)) {B : Type} (hb : hasType (maybe A ctx) b B)
  : hasType ctx (b >>= maybe a var) B => {?}

-- 9. Докажите, что если Г |- a : A и Red a a', то Г |- a' : A

\func redLem {V : \Type} {ctx : V -> Type} {a a' : Term V} (r : Red a a') {A : Type} (h : hasType ctx a A)
  : hasType ctx a' A \elim r
  | clos-refl p => rewriteF p h
  | clos-trans (t, r1, r) => redLem r (red1Lem r1 h)
  \where {
    \func red1Lem {V : \Type} {ctx : V -> Type} {a a' : Term V} (r : Red1 a a') {A : Type} (h : hasType ctx a A)
    : hasType ctx a' A \elim a, a', r, A, h
      | lam t, lam s, lamRed r, Arr A B, lamType h => lamType (red1Lem r h)
      | app t1 t2, app s1 s2, appLeft t1=>s1 t2=s2, B, appType h1 h2 => appType (rewriteF t2=s2 h1) (red1Lem t1=>s1 h2)
      | app t1 t2, app s1 s2, appRight t1=s1 t2=>s2, B, appType h1 h2 => appType (red1Lem t2=>s2 h1) (rewriteF t1=s1 h2)
      | app f a, s, beta f1 p p1, B, appType h1 h2 => run {
        rewriteI p at h2,
        \case h2 \with {
          | lamType h => rewriteF p1 (substLem1 a h1 f1 h)
        }
      }
  }

-- Optional
-- 10. Докажите typeableInterp из лекции