\import Logic.Meta
\import Meta
\import Paths.Meta
\import lect07
\import lect09
\import Utils \hiding (<=-refl)

-- 1. Пусть f : A -> B и g : B -> C ─ некоторые функции.
--    Докажите, что если f и g инъективны, то g `o` f также инъективна.
--    Докажите, что если g `o` f инъективна, то f также инъективна.

-- Композиция функций
\func \fixr 9 o {A B C : \Type} (g : B -> C) (f : A -> B) => \lam a => g (f a)

\func o-inj {A B C : \Type} (f : A -> B) (g : B -> C) (p : isInj f) (q : isInj g) : isInj (g `o` f) =>
  \lam a a' p1 => p a a' (q (f a) (f a') p1)

\func o-inj' {A B C : \Type} (f : A -> B) (g : B -> C) (p : isInj (g `o` f)) : isInj f =>
  \lam a a' p1 => p a a' (pmap g p1)


-- 2. Определите предикат "делится на 3 или на 5" так, чтобы он возвращал утверждения.
--    Докажите, что MultipleOf3Or5 вкладывается в ℕ.

\func isGG (n : Nat) : Bool => \case mod3 n, mod5 n \with {
  | 0, 0 => true
  | 0, suc n1 => true
  | suc n1, 0 => true
  | suc n1, suc n2 => false
}

\func isMultipleOf3Or5 (n : Nat) : \Type => T (isGG n)

\func isMultipleOf3Or5-isProp (n : Nat) : isProp (isMultipleOf3Or5 n) => \lam x y => T-lem

\func MultipleOf3Or5 => \Sigma (n : Nat) (isMultipleOf3Or5 n)

\func Mul-inc (m : MultipleOf3Or5) => m.1

\func Mul-inc-isInj : isInj Mul-inc => \lam a a' p => sigmaEq (\lam n => T (isGG n))  a a' p T-lem

-- 3. Мы будем говорить, что тип A тривиален, если существует элемент в A, такой что любой другой элемент в A равен ему.
--    Докажите, что тип A тривиален тогда и только тогда, когда A является утверждением и A населен.

\func isTriv (A : \Type) => \Sigma (a : A) (\Pi (a' : A) -> a = a')

\func \infix 1 <-> (A B : \Type) => \Sigma (A -> B) (B -> A)

\func isTriv-lem (A : \Type) : isTriv A <-> (\Sigma (isProp A) A) =>
  (\lam i => (\lam x y => sym (i.2 x) *> i.2 y, i.1), \lam _x => (_x.2, \lam a' => _x.1 _x.2 a'))

-- 4. Докажите, что Either не является утверждением в общем случаем.

\func Either-isProp (p : \Pi {A B : \Type} (pA : isProp A) (pB : isProp B) -> isProp (Either A B)) : Empty =>
  \case ((p Unit-isProp Unit-isProp) (Either.inl ()) (Either.inr ()))

-- 5. Докажите, что LessOrEq является предикатом.

\data LessOrEq (n m : Nat) : \Set0 \with
  | 0, m => z<=n
  | suc n, suc m => s<=s (LessOrEq n m)

\func LessOrEq-isProp {n m : Nat} : isProp (LessOrEq n m) => aux
  \where {
    \func aux {n m : Nat} (x y : LessOrEq n m) : x = y
      | {0}, z<=n, z<=n => idp
      | {suc n}, {suc m}, s<=s x, s<=s y => rewrite (aux x y) idp
  }

-- 6. Докажте, что ReflClosure LessOrEq не является предикатом, а ReflClosure (\lam x y => T (x < y)) является.

\func \infix 4 < (n m : Nat) : Bool
  | _, 0 => false
  | 0, suc _ => true
  | suc n, suc m => n < m

\data ReflClosure (R : Nat -> Nat -> \Type) (x y : Nat)
  | refl (x = y)
  | inc (R x y)

\func ReflClosure_<-isProp (n m : Nat) : isProp (ReflClosure (\lam x y => T (x < y)) n m) => aux n m
\where {
  \func aux (n m : Nat) (x y : ReflClosure (\lam x y => T (x < y)) n m) : x = y \elim x, y
    | refl p, refl p1 => pmap refl (Dec-isSet decideEq n m p p1)
    | refl p, inc r => \case lemma p r
    | inc r, refl p => \case lemma p r
    | inc r, inc r1 => pmap inc T-lem

  \func lemma {n m : Nat} (p : n = m) (q  : T (n < m)) : Empty
    | {0}, {0}, p, ()
    | {suc n}, {suc m}, p, q => lemma (pmap unsuc p) q
  \func unsuc (n : Nat) : Nat
    | 0 => 0
    | suc n => n
}

\func ReflClosure_<=-isNotProp (p : \Pi (n m : Nat) -> isProp (ReflClosure LessOrEq n m)) : Empty => \case (p 0 0 (refl idp) (inc z<=n))

-- 7. Докажите, что если тип A вкладывается в тип B и B является утверждением, то и A является утверждением.

\func sub-isProp {A B : \Type} (f : A -> B) (p : isInj f) (q : isProp B) : isProp A => \lam x y => p x y (q (f x) (f y))

-- 8. Докажите, что тип с разрешимым равенством являетя множеством.

\func Dec-isSet {A : \Type} (dec : \Pi (x y : A) -> Decide (x = y)) : isSet A
  => \lam x y => {?}
  \where {
    \func toBool {A : \Type} {x y : A} (d : Decide (x = y)) : Bool
      | yes a => true
      | no _x => false
    \func from {A : \Type} {x y : A} (dec : \Pi (x y : A) -> Decide (x = y)) (p : x = y) : T (toBool (dec x y))
      | dec, idp => {?}
    \func to {A : \Type} {x y : A} (dec : \Pi (x y : A) -> Decide (x = y)) (t : T (toBool (dec x y))) : x = y
  }

-- 9. Если A и B являются множествами, то A `Or` B тоже является множеством.

\data \fixr 2 Or (A B : \Type)
  | inl A
  | inr B

\func inlOrDef {A B : \Type} (def : A) (o : Or A B)  : A \elim o
  | inl a => a
  | inr b => def
      -- path (\lam i => inl (inlOrDef a (x @ i))) = x

\func or-isSet {A B : \Type} (p : isSet A) (q : isSet B) : isSet (Or A B)
  => \lam a a' => \case \elim a, \elim a' \with {
    | inl a, inl a' => retract-isProp (p a a') (pmap (inlOrDef a)) (pmap inl) (\lam x => rewrite (fu x inl (inlOrDef a)) {?})
    | inl a, inr b => contradiction
    | inr b, inl a => contradiction
    | inr b, inr b' => {?}
  }


\func fu {A B C : \Type} {a a' : A} (p : a = a') (f : B -> C) (g : A -> B)  : pmap f (pmap g p) = pmap (\lam x => f (g x)) p
  | idp, g, f => idp


-- 10. Если B x является множеством, то \Pi (x : A) -> B x тоже является множеством.

\func pi-isSet {A : \Type} (B : A -> \Type) (p : \Pi (x : A) -> isSet (B x)) : isSet (\Pi (x : A) -> B x) => {?}

-- 11. Докажите, что если A является множеством, то List A также им является.

\func List-isSet {A : \Type} (pA : isSet A) : isSet (List A)
  => {?}

-- Optional:
-- 12. Докажите, что n-типы замкнуты относительно образования \Pi-типов.
--     Hint: Доказательство по индукции. Для случая suc n нужно доказать, что если f,g : \Pi (x : A) -> B x, то f = g эквивалентно \Pi (x : A) -> f x = g x.

\func levelPi {A : \Type} (B : A -> \Type) (n : Nat) (p : \Pi (x : A) -> B x `hasLevel` n) : (\Pi (x : A) -> B x) `hasLevel` n => {?}